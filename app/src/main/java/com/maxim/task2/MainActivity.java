package com.maxim.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText text;
    Button button_delete;
    Button button_returnText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.editText_text);
        final String[] wroteText = {text.getText().toString()};

        button_delete = (Button) findViewById(R.id.button_delete);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wroteText[0] = text.getText().toString();
                text.setText("");
            }
        });

        button_returnText = (Button) findViewById(R.id.button_return_text);
        button_returnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setText(wroteText[0]);

            }
        });
    }

}
